#include "led_portD.h"

#define button_PORT PORTD
#define button_DIR  DDRD
#define button_PIN	PIND

#define bt1 PIND3	
#define bt2 PIND4
#define bt3 PIND5
unsigned char setup,temp_pin_stt;
/************************************************************************/
/* Button init                                                      */
/************************************************************************/
void button_init(void)
{
	//----------------------------------------
	button_PORT|=(1<<bt3)|(1<<bt2)|(1<<bt1);//pull up all the buttons logic
	button_DIR&=(~(1<<bt3))&(~(1<<bt2))&(~(1<<bt1));//set all buttons are input
	//----  bt1 is an external interrupt ------
	sei();
	MCUCR|=(1<<ISC11)|(1<<ISC10);
	GICR|=1<<INT1;
	//-----------------------------------------
}

unsigned char get_button_stt(unsigned char button_pin)
{
	unsigned char button_stt;
	button_stt= (button_PIN&(1<<button_pin))>>button_pin;
	return button_stt;
}


ISR(INT1_vect)
{
	setup++;
	setup%=2;
	if(setup==1)
	{
	led_1(on);
	}
	else
	{
	led_1(off);
	}
}
