#define on 0
#define off 1

void led_pb0(unsigned char i)
{
	i%=2;
	DDRB|=1<<PINB0;
	if(i==on)PORTB&=~(1<<PINB0);
	if(i==off)PORTB|=1<<PINB0;
}