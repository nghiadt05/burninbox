/*
 * display.c
 *
 * Created: 6/11/2013 8:29:55 AM
 *  Author: nghia
 */ 
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <mylib/atmega16/usart.h>

#include "button.h"
#include "lcd20x4.h"


float des_temp=24.5,avg_temp=20,pressure=112.5;
unsigned char i,j;

int main(void)
{
	//------ system init ------------------
	lcd_init();
	button_init();
	usart_init();
	led_1(off);
	led_2(off);
	//-------------------------------------
	//---------- welcome note -----------------------	
	lcd_set_cs(1,5);
	lcd_send_string("Burn-in Box");
	lcd_set_cs(3,0);
	lcd_send_string("A VNPTtechnolgy's ");
	lcd_set_cs(4,0);
	lcd_send_string("simulation product");
	lcd_clr();
	//------------------------------------------------
	setup=0;	
    while(1)
    {
		/*
      	if(setup==0)
		  {
			  //----- display into lcd20x4--------
			lcd_set_cs(1,0);
			lcd_send_string("Des_Temp: ");
			lcd_print_rnb(des_temp);
			lcd_send_string(" oC");
			lcd_set_cs(2,0);
			lcd_send_string("Temp: ");
			lcd_print_rnb(avg_temp);
			lcd_send_string(" oC");
			lcd_set_cs(3,0);
			lcd_send_string("Pressure: ");
			lcd_print_rnb(pressure);
			lcd_send_string("kPa");		
			//----------------------------------
		  }			
		  else//setup=1
		  {
			lcd_set_cs(1,0);
			lcd_send_string("Set destination temp");
			lcd_set_cs(2,5);
			lcd_print_rnb(des_temp);
		  }		
		  
		  for(i=0;i<200;i++)
		  {
			  temp_pin_stt=get_button_stt(bt2);
		  }  
		  if(temp_pin_stt==0)des_temp-=0.1;
		  
		  for(i=0;i<200;i++)
		  {
			  temp_pin_stt=get_button_stt(bt3);
		  }  
		  if(temp_pin_stt==0)des_temp+=0.1;
		  
		//------- lcd loop clear screen -------
		for(loop_clr=0;loop_clr<200;loop_clr++)
		  {
			  if(loop_clr==199)lcd_clr();
			  _delay_ms(1);
		  }  
		//-------------------------------------
	    */
		
		//----- test data transmit by usart -------
		lcd_set_cs(2,3);
		lcd_print_rnb(des_temp);
		//-----------------------------------------
		//------- lcd loop clear screen -------
		for(loop_clr=0;loop_clr<200;loop_clr++)
		  {
			  if(loop_clr==199)lcd_clr();
			  _delay_ms(1);
		  }  
		//-------------------------------------
		}
		
}

ISR(USART_RXC_vect)
{
	rx_count_number++;
	rx_data[rx_count_number-1]=UDR;
	if(rx_count_number==4)//nhan xong
	{
		des_temp=(float) (rx_data[0]|(rx_data[1]<<4)|(rx_data[2]<<8)|(rx_data[3]<<12));
		rx_count_number=0;
	}
	
}