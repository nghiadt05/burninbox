#define on 0
#define off 1

void led_1(unsigned char i)
{
	i%=2;
	DDRD|=1<<PIND6;
	if(i==on)PORTD&=~(1<<PIND6);
	if(i==off)PORTD|=1<<PIND6;
}

void led_2(unsigned char i)
{
	i%=2;
	DDRD|=1<<PIND7;
	if(i==on)PORTD&=~(1<<PIND7);
	if(i==off)PORTD|=1<<PIND7;
}