# Abstract#

The main idea of this project is to make use of the data of various types of sensors ,which are a group of 4 temperature (DS18B20) and 4 humidity sensors (DHT11) located in the corners of a room, as the input for the actuator controls (fans or air-conditioners and heaters) to maintain a preset temperature inside that room. Besides, a smoke sensor is also used to signal an alarm when a fire happens. 

Basically, it is only a small demonstration of a controlling algorithm for electronic devices when I took part in an experimental electronic class.

This project contains the coding part and all related PCB designs for the whole system.

All component:

![15988_594131070638913_1362650124_n.jpg](https://bitbucket.org/repo/6pdRqB/images/3244230788-15988_594131070638913_1362650124_n.jpg)

The complete "Burn In Box":

![3509_474884769230211_1282258772_n.jpg](https://bitbucket.org/repo/6pdRqB/images/994726384-3509_474884769230211_1282258772_n.jpg)