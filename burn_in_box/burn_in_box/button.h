#include "led_portB.h"

#define button_PORT PORTB
#define button_DIR  DDRB
#define button_PIN	PINB

#define bt1 PINB0		
#define bt2 PINB1
#define bt3 PINB2
unsigned char setup,temp_pin_stt;
/************************************************************************/
/* Button init                                                      */
/************************************************************************/
void button_init(void)
{
	//----------------------------------------
	button_PORT|=(1<<bt3)|(1<<bt2)|(1<<bt1);//pull up all the buttons logic
	button_DIR&=(~(1<<bt3))&(~(1<<bt2))&(~(1<<bt1));//set all buttons are input
	//----  bt1 is an external interrupt ------
	PCICR|=1<<PCIE0;
	PCMSK0|=1<<PCINT0;
	//-----------------------------------------
}

unsigned char get_button_stt(unsigned char button_pin)
{
	unsigned char button_stt;
	button_stt= (button_PIN&(1<<button_pin))>>button_pin;
	return button_stt;
}

unsigned char get_button1_stt(void)
{
	unsigned char button_stt;
	button_stt= (button_PIN&(1<<bt1))>>bt1;
	return button_stt;
}


ISR(PCINT0_vect)
{
	_delay_us(1);
	temp_pin_stt=get_button1_stt();
	if(temp_pin_stt==0)//button is pressed
	{
		setup++;
		setup%=2;
		if(setup==1)
		{
		led_1(on);
		led_2(on);
		}
		else
		{
		led_1(off);
		led_2(off);
		}
	}
	else//button release
	{}
}
