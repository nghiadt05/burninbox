#include <avr/io.h>

unsigned char ds1822_pin, pin_stt;
unsigned char ds1822_read_data[9];
unsigned char ds1822_write_data[8];
unsigned char temp_l,temp_h;
unsigned char integer,decimal;
unsigned char mod_reg;
unsigned char rom_code[8];
unsigned char check_crc;
unsigned char scraft_data[9];

unsigned char sensor_addr[4][8]={40,48,197,184,0,0,0,142,
								 40,49,197,184,0,0,0,185,
								 40,50,197,184,0,0,0,224,
								 40,51,197,184,0,0,0,215};
const char crc_check_table[256] =
{
 0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
 157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
 35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60,98,
 190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
 70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89,7,
 219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 
154,101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 
36, 248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 
185, 140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 
205, 17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14,
80, 175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 
238, 50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 
115, 202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 
139, 87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 
22, 233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 
168, 116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107,53};
	
float temp;
#define DS1822_PORT PORTC
#define DS1822_PIN	PINC
#define DS1822_DIR  DDRC
#define temp_error 127

//#define _9_bits_resolution
//#define _10_bits_resolution
//#define _11_bits_resolution
#define _12_bits_resolution

#ifdef _9_bits_resolution
	#define decimal_step 0.5
#endif

#ifdef _10_bits_resolution
	#define decimal_step 0.25
#endif

#ifdef _11_bits_resolution
	#define decimal_step 0.125
#endif

#ifdef _12_bits_resolution
	#define decimal_step 0.0625
#endif

//--------- needed command ----------
#define DS1822_CMD_CONVERTTEMP		0x44
#define DS1822_CMD_RSCRATCHPAD		0xbe
#define DS1822_CMD_WSCRATCHPAD		0x4e
#define DS1822_CMD_CPYSCRATCHPAD	0x48
#define DS1822_CMD_RECEEPROM		0xb8
#define DS1822_CMD_RPWRSUPPLY		0xb4
#define DS1822_CMD_SEARCHROM		0xf0
#define DS1822_CMD_READROM			0x33
#define DS1822_CMD_MATCHROM			0x55
#define DS1822_CMD_SKIPROM			0xcc
#define DS1822_CMD_ALARMSEARCH		0xec
//-----------------------------------

void ds1822_choose_pin(unsigned char i)// [ok]
{
	i%=8;
	ds1822_pin=i;
}

void ds1822_input(void)// set ds1822 pin to input mode	[ok]
{
	DS1822_DIR&=~(1<<ds1822_pin);
}

void ds1822_output(void)// set ds1822 to output mode	[ok]
{
	DS1822_DIR|=(1<<ds1822_pin);
}

unsigned char ds1822_read_pin_stt(void)// read DS1822 status bit	[ok]
{
	ds1822_input();
	pin_stt=(DS1822_PIN&(1<<ds1822_pin))>>ds1822_pin;
	return pin_stt;
}

void ds1822_write_pin(unsigned char i)// write 1 or 0 to DQ line	[ok]
{
	ds1822_output();
	if(i==0)
	{
		DS1822_PORT&=~(1<<ds1822_pin);// write 0
	}	
	else
	{
		DS1822_PORT|=(1<<ds1822_pin);// write 1
	}
	
}

void ds1822_reset(void)	// [ok]
{
	//-------- pull down DQ_pin in 480us ---------
	ds1822_write_pin(0);
	_delay_us(480);
	//---------------------------------------------
	
	// release the line then wait for 60us to ensure
	// that ds1822 can caculate the call
	ds1822_input();
	_delay_us(60);
	//---------------------------------------------
	
	//store the value of DQ_line, then wait 420us
	//to complete the 480 us long
	ds1822_read_pin_stt();
	_delay_us(420);
	//----------------------------------------------
	// if pin_stt=0: ok; else fall
}

void master_write_bit(unsigned char i)
{
	if(i>1)i=1;
	ds1822_write_pin(0);// need 1us pull_down in DQ line in each individual write
	_delay_us(1);
	
	if(i==1)// if we want to write 1 to DQ_line, release it now
	{
		ds1822_input();
	}
	
	// else we wait to write 0 to DQ_line, wait at least 60us then release DQ_line
	_delay_us(60);
	ds1822_input();
}

void master_write_byte(unsigned char byte)// write LSB first, MSB last
{	
	unsigned char i;
	for(i=0;i<8;i++)
	{
		master_write_bit((byte&(1<<i))>>i);
	}
}

unsigned char master_read_bit(void)// read a bit in DQ_line
{
	unsigned char i;
	// pull down DQ_line in 1us
	ds1822_write_pin(0);
	_delay_us(1);
	
	// release the DQ_line and wait 19us to ensure the line is rally released
	ds1822_input();
	_delay_us(14);
	
	// read the DQ_line status then wait for 45us to end the write zero bit; 
	// write 1 bit no need to wait for 45us
	i=ds1822_read_pin_stt();// if i=0 we read as 0; if i=1 we read as 1
	_delay_us(45);
	
	return i;
}

unsigned char master_read_byte(void)// // read LSB first, MSB last
{
	unsigned char i=8, byte=0;
		while(i--)
		{
		//Shift one position right and store read value
		byte>>=1;
		byte|=(master_read_bit()<<7);
		}
	return byte;
}

void config_modreg(unsigned char i)//i= the numbers of resolution bits: throught 9 to 12
{
	if(i<9){i=9;}
		else if(i>12){i=12;}
			else{}					
	mod_reg= 0x1f|((i-9)<<5);	
}


/************************************************************************/
/* Read rom code                                          */
/************************************************************************/
void ds1822_read_rom_code(void)
{
	unsigned char i;
	ds1822_reset();
	master_write_byte(DS1822_CMD_READROM);
	
	for(i=0;i<8;i++)
	{
		rom_code[i]=master_read_byte();
	}
}

float ds1822_get_temp_single_sensor(void)
{	
	
	unsigned char i;
measure_loop:
	// reset, skip rom and start a conversion
	ds1822_reset();
	master_write_byte(DS1822_CMD_SKIPROM);
	master_write_byte(DS1822_CMD_CONVERTTEMP);
	
	// wait until a single conversion complete
	while(!master_read_bit());
	
	//Reset, skip ROM and send command to read Scratchpad
	ds1822_reset();
	master_write_byte(DS1822_CMD_SKIPROM);
	master_write_byte(DS1822_CMD_RSCRATCHPAD);
	
	// Read temp_l and temp_h byte
	//temp_l=master_read_byte();
	//temp_h=master_read_byte();
	check_crc=0;
	for(i=0;i<9;i++)
	{
		scraft_data[i]=master_read_byte();
		check_crc=crc_check_table[check_crc^scraft_data[i]];
	}
	ds1822_reset();
	
	if(check_crc==0)
	{
		// caculate the receive value
		integer=(scraft_data[0]>>4)|((scraft_data[1]&0x07)<<4);
		decimal=scraft_data[0]&0x0f;
		temp=((float) integer)+ ((float)decimal*decimal_step);
		return temp;
	}
	else 
	{
	goto measure_loop;
	}	
}

float ds1822_get_temp_multi_sensors(unsigned char sensor_number)
{
	unsigned char i;
measure_loop:
	ds1822_choose_pin(0);
	ds1822_reset();
	master_write_byte(DS1822_CMD_MATCHROM);
	
	//----- send the correct rom code for each sensor -----
	for(i=0;i<8;i++)
	{
		master_write_byte(sensor_addr[sensor_number][i]);
	}
	//----------------------------------------------------
	
	master_write_byte(DS1822_CMD_CONVERTTEMP);// star a conversion
	while(!master_read_bit());// wait until a single conversion complete
	
	//--------------- read temperature data from specific ds18b20 ------
	ds1822_reset();// reset
	master_write_byte(DS1822_CMD_MATCHROM);// send match rom code command
	//----- send the correct rom code for each sensor -----
	for(i=0;i<8;i++)
	{
		master_write_byte(sensor_addr[sensor_number][i]);
	}
	//----------------------------------------------------	
	master_write_byte(DS1822_CMD_RSCRATCHPAD);
		
	// Read 9 bytes with temp_l and temp_h byte
	check_crc=0;
	for(i=0;i<9;i++)
	{
		scraft_data[i]=master_read_byte();
		check_crc=crc_check_table[check_crc^scraft_data[i]];
	}
	ds1822_reset();
	//-----------------------------------------
	
	// caculate the receive value if we receive the correct crc byte -----
	if(check_crc==0)
	{
		// caculate the receive value
		integer=(scraft_data[0]>>4)|((scraft_data[1]&0x07)<<4);
		decimal=scraft_data[0]&0x0f;
		temp=((float) integer)+ ((float)decimal*decimal_step);
		return temp;
	}
	else 
	{
	goto measure_loop;
	}
	//---------------------------------------------------------------------
}