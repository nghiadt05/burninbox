#define on 0
#define off 1
#define led1_pin	PINB3
#define led2_pin	PINB4

void led_1(unsigned char i)
{
	i%=2;
	DDRB|=1<<led1_pin;
	if(i==on)PORTB&=~(1<<led1_pin);
	if(i==off)PORTB|=1<<led1_pin;
}

void led_2(unsigned char i)
{
	i%=2;
	DDRB|=1<<led2_pin;
	if(i==on)PORTB&=~(1<<led2_pin);
	if(i==off)PORTB|=1<<led2_pin;
}