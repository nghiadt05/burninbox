/*
 * ds1822.c
 *
 * Created: 6/6/2013 7:38:02 PM
 *  Author: Administrator
 */ 
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

#include "ds1822.h"
#include "mpx4115.h"
#include "control_heater.h"
#include "usart_ds1820.h"
#include "seperate_data.h"
#include "button.h"
#include "lcd20x4.h"

#define sensor_number 4
#define warm_up
#define lcd_mode
#define usart_mode

unsigned char j,i;
float t_temp[4];


int main(void)
{
	CLKPR=0x80;
	CLKPR=0x00;
	//--------- system init ------------
	cli();//prevent interrupt
	adc_init();
	heater_init();
	usart_init();
	button_init();
	//----------------------------------
	
	//--- let do some warm up measurement to against noise ---
	#ifdef warm_up
	for(j=0;j<2;j++)
	{
		for(i=0;i<sensor_number;i++)
		{
			ds1822_get_temp_multi_sensors(i);	
		}
	}
	    //----- getting temperature value ---
		for(i=0;i<sensor_number;i++)
		{
			ds1822_get_temp_multi_sensors(i);	
			if(temp<temp_error)
			{
			t_temp[i]=temp;
			}
			else{}
		}
		avg_temp=(float)(t_temp[0]+t_temp[1]+t_temp[2]+t_temp[3])/4;
		des_temp=(int)avg_temp;//destination temperature= present temperature, so we won't do anything
		//-----------------------------------
	#endif
	//--------------------------------------------------------
	sei();//enable interrupt
	setup=0;
	heater_start_count();
	//--------------------------------------------------------	
    while(1)
    {
		if(setup==0)
		{
		//----- getting pressure value ------
		adc_get_value(1);
		mpx4115_get_pressure();
		//-----------------------------------
		
		//----- getting temperature value ---
		for(i=0;i<sensor_number;i++)
		{
			ds1822_get_temp_multi_sensors(i);	
			if(temp<temp_error)
			{
			t_temp[i]=temp;
			}
			else{}
		}
		avg_temp=(float)(t_temp[0]+t_temp[1]+t_temp[2]+t_temp[3])/4;
		get_t_on();
		//-----------------------------------
		
		#ifdef usart_mode
		usart_send_string("temp[oC]= ");
		usart_print_rnb(avg_temp);
		usart_send_string(" pressure[kPa]= ");
		usart_print_rnb(pressure);
		usart_xuongdong();
		#endif
		}
		else//setup=1: change the destination tmperature
		{
		//------------  check bt2 is pressed ??? ----------
			for(i=0;i<100;i++)
			{
			temp_pin_stt=get_button_stt(bt2);
			}
			if(temp_pin_stt==0){des_temp-=0.1;}
		//-------------------------------------------------	
		
		//------------  check bt3 is pressed ??? ----------
			for(i=0;i<100;i++)
			{
			temp_pin_stt=get_button_stt(bt3);
			}
			if(temp_pin_stt==0){des_temp+=0.1;}
		//-------------------------------------------------
			_delay_ms(5);
			
		#ifdef usart_mode	
		usart_send_string("Des_temp[oC]: ");
		usart_print_rnb(des_temp);
		usart_xuongdong();		
		#endif	
		}	
    }
}