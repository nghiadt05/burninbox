#define T 5000// 5000 ms
float delta_t,avg_temp,des_temp;
unsigned int t_on,t_count;
#define TCNT0_reset 6
//--- let timer0 running in free mode, in each 1ms timer0 overflows once
// then we store the count of overflow time of timer0, so we have time in ms
// the control period is 5s=5000ms=T, when t_count=5000ms we reset t_count become zero
// case1: 0<=t_count<t_on: heater on
// case2: t_on<=t_count<T: heater off
// by caculating the delta_t= des_temp-avg_temp we'll get t_on like those equation:
//		delta_t<1oC: t_on=1s=1000ms
//		delta_t<2oC: t_on=2s=2000ms
//		delta_t<3oC: t_on=3s=3000ms
//		delta_t<4oC: t_on=4s=4000ms
//		delta_t>=4oC: t_on=5s=5000ms

void heater_init(void)
{
	DDRC|=(1<<PINC2);//heater= PINC2
	heater_off();
	//---- timer0 init ----------
	TCCR0A= 0x00;	
	TCNT0 = 6;
	TIMSK0|= (1<<TOIE0);	
	TCCR0B= 0;
	//---------------------------
}

void heater_start_count(void)
{
	TCCR0B|= (1<<CS01)|(1<<CS00);	
}
unsigned int get_t_on(void)
{
	delta_t=des_temp-avg_temp;
	if(delta_t<0||delta_t==0)
	{
		t_on=0;
	}
	else//delta_t>0
	{
		if(delta_t<1)
		{
		t_on=1000;//ms
		}	
		else if(delta_t<2)
		{
		t_on=2000;
		}
		else if(delta_t<3)
		{
		t_on=3000;
		}
		else if(delta_t<4)
		{
		t_on=4000;
		}
		else
		{
		t_on=5000;
		}
	}		
	return t_on;	
}

void heater_off(void)
{
	PORTC|=1<<PINC2;
}

void heater_on(void)
{
	PORTC&=~(1<<PINC2);
}

//----- sub program to caculate the system time in each 10us
ISR(TIMER0_OVF_vect)
{
   t_count+=1;
   if(t_count==T)t_count=0;
   
   if(t_on>t_count)heater_on();
   if(t_on<t_count || t_on==t_count)heater_off();
   TCNT0=TCNT0_reset;
}