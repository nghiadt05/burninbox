unsigned char rx_send_data[50];
unsigned char rx_receive_data[50];

void seperate_data(float y,unsigned char pointer)// seperate a float variable to 4 bytes unsigned char 
												 // pointer is a start position of rx_send_data[] array
												// send least string byet first, most string byte last
{
	rx_send_data[pointer]  = (unsigned char)((unsigned long)y & 0x000f)>>0;
	rx_send_data[pointer+1]= (unsigned char)((unsigned long)y & 0x00f0)>>4;
	rx_send_data[pointer+2]= (unsigned char)((unsigned long)y & 0x0f00)>>8;
	rx_send_data[pointer+3]= (unsigned char)((unsigned long)y & 0xf000)>>12;	
}