/************************************************************************/
/*	USART INIT
1. do not use multiprocessors mode                                    
2. use 8 data bits, 1 start bt and 1 stop bit
/************************************************************************/
#define Baud_rate_11520 8
#define Baud_rate_9600	103
void usart_init(void)
{
	UBRR0H=0;
	UBRR0L=Baud_rate_11520;
	UCSR0A=0;
	UCSR0C|=(1<<UCSZ01)|(1<<UCSZ00);//asynchronous usart, diasbled parity mode, 1 stop bit, charactor size: 8 bits
	UCSR0B|=(1<<RXEN0)|(1<<TXEN0);//cho phep ngat nhan, cho phep truyen, nhan	
}
/************************************************************************/
/* usart receive                                                        */
/************************************************************************/
unsigned char usart_receive(void)
{
   return UDR0;
}

/************************************************************************/
/* Usart transmit                                                       */
/************************************************************************/
void usart_transmit(unsigned char data)
{
	loop_until_bit_is_set(UCSR0A,UDRE0);
	UDR0=data;
}


/************************************************************************/
/* usart send string                                                    */
/************************************************************************/
void usart_send_string (unsigned char const *string)// lenh in 1 xau ki tu *string
{
  while (*string)
  {
    usart_transmit(*string++); //in ra gia tri 1 ki tu cua xau, dc tro boi con tro *string
  }
}
/*******************************************************************************
* In so nguyen co 5 chu so                                                *
*******************************************************************************/
void usart_print_nb(unsigned long n)//in so co 5 chu so ko co dau phay
{
	unsigned long a,b;
	if(n<10){usart_transmit(n+48);}//so co 1 chu so
	  else 
	if(n<100)//so co 2 chu so
	{
		usart_transmit(n/10+48);//in chu so hang chuc
		usart_transmit(n%10+48);//in chu so hang don vi
	}
	
	  else
	if(n<1000)//so co 3 chu so
	{
		a=n/10;//a chua chu so hang tram va hang chuc
		usart_transmit(a/10+48);//in chu so hang tram
		usart_transmit(a%10+48);//in chu so hang chuc
		usart_transmit(n%10+48);//in chu so hang don vi
		
	}
	 else if(n<10000)//so co 4 chu so 
	{
		a=n/100;//chua hang nghin va hang tram
		b=n%100;//chua hang chuc va don vi
		usart_transmit(a/10+48);//in so hang nghin
		usart_transmit(a%10+48);//in so hang tram
		usart_transmit(b/10+48);//in so hang chuc
		usart_transmit(b%10+48);//in so hang don vi
		
	}	
	 else if(n<100000)//so co 5 chu so
	 {
		a=n/1000;//chua hang chuc nghin va hang nghin
		b=n%1000;//chua hang tram,chuc va don vi
		usart_transmit(a/10+48);//in so hang chuc nghin
		usart_transmit(a%10+48);//in so hang nghin
		usart_transmit(b/100+48);//in so hang tram
		usart_transmit((b/10)%10+48);//in so hang don chuc
		usart_transmit(b%10+48);//in so hang don vi
		
	 }		 
		
}

/*******************************************************************************
* In so co 3 chu so truoc dau phay, 3 chu so sau dau phay                                                *
*******************************************************************************/
void usart_print_rnb(float r)//in so thap phan co 4 chu so sau dau phay
{
	unsigned long p_nguyen;
	unsigned int t_phan;
	
	if(r<0)// them dau tru neu la so am
	{
	usart_transmit(45);
	r=-r;
	}
	p_nguyen=r/1;
	t_phan=(r-p_nguyen)*1000/1;//lay ba chu so sau dau phay, sau do in ra ca 3 ki tu so nay
	usart_print_nb(p_nguyen);
	usart_transmit(46);
	usart_transmit((t_phan/100)+48);
	usart_transmit((t_phan%100)/10+48);
	usart_transmit(t_phan%10+48);
}

/************************************************************************/
/* Xuong dong                                                           */
/************************************************************************/
void usart_xuongdong(void)
{
	usart_transmit(13);
}

//------------------------------------------------------------------------
void usart_clr(unsigned char y)
{
	unsigned char i;
	for(i=0;i<y;i++)
	{
		usart_transmit(0x08);
	}
}

//-------- all convert to hexa then print  -------------
void usart_print_hexnb(int x_var)
{
	unsigned char x[4],i;	
	//x[4]=(x_var&0xf0000)>>16;
	x[3]=(x_var&0xf000)>>12;
	x[2]=(x_var&0xf00)>>8;
	x[1]=(x_var&0xf0)>>4;
	x[0]=(x_var&0x0f);
	for(i=4;i>0;i--)
	{
		if(x[i-1]>10 || x[i-1]==10){usart_transmit(x[i-1]+55);}
			else{usart_transmit(x[i-1]+48);}
		//if(i==3){usart_transmit(32);}
	}	
	
}

/************************************************************************/
/* usart print int number to decimal type                               */
/************************************************************************/
void usart_print_int(int a)
{
	if((a&0x8000)==0)//so duong
		{
			usart_print_nb(a);
		}		
	else//so am
		{
			usart_transmit(45);
			usart_print_nb(65536-(unsigned int)a);
		}		
}

