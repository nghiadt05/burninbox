unsigned char adc_loop;
unsigned long adc_value;
float pressure;
#define VREF 5
/************************************************************************/
/* analog to digital init: just use adc[0:3]
 1.use 8 MSB to again nosie	   
 2. Vref= AVcc       
 3. disable digital input adc[0:3]                              */
/************************************************************************/
void adc_init(void)
{
	//ADMUX|=(1<<REFS0);
	ADCSRA|=(1<<ADEN)|(1<<ADPS2);//f_adc=f_sys/4=4 Mhz
	//DIDR0|=0x0f;
}

/************************************************************************/
/* analog to digital conversion						            */
/************************************************************************/
unsigned int adc_get_value(unsigned char chanel)
{
	adc_value=0;
	ADMUX&=(~(0x0f));// clear 4 address analog bit
	ADMUX|=(chanel & 0x0f);// chose what analog chanel will be converted
	for(adc_loop=0;adc_loop<100;adc_loop++)
	{
		ADCSRA|=(1<<ADSC);//start conversion
		loop_until_bit_is_set(ADCSRA,ADIF);//wait until a conversion complete
		adc_value+=ADCL;
		adc_value+=ADCH*256;
		_delay_us(5);
	}
	adc_value=adc_value/100;	
	return adc_value;
}

float mpx4115_get_pressure(void)
{
	pressure=((float)adc_value/(float)1024+0.095)/0.009;
	//pressure=(float)adc_value*(float)VREF/(float)1024;
	return pressure;
}