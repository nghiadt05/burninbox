#include <avr/io.h>

unsigned char ds1822_pin, pin_stt;
unsigned char ds1822_read_data[9];
unsigned char ds1822_write_data[8];
unsigned char temp_l,temp_h;
unsigned char integer,decimal;
unsigned char mod_reg;
unsigned char rom_code[8];

unsigned char sensor_addr[4][8]={40,48,197,184,0,0,0,142,
								 40,49,197,184,0,0,0,185,
								 40,50,197,184,0,0,0,224,
								 40,51,197,184,0,0,0,215};

	
float temp;
#define DS1822_PORT PORTC
#define DS1822_PIN	PINC
#define DS1822_DIR  DDRC
#define temp_error 127

//#define _9_bits_resolution
//#define _10_bits_resolution
//#define _11_bits_resolution
#define _12_bits_resolution

#ifdef _9_bits_resolution
	#define decimal_step 0.5
#endif

#ifdef _10_bits_resolution
	#define decimal_step 0.25
#endif

#ifdef _11_bits_resolution
	#define decimal_step 0.125
#endif

#ifdef _12_bits_resolution
	#define decimal_step 0.0625
#endif

//--------- needed command ----------
#define DS1822_CMD_CONVERTTEMP		0x44
#define DS1822_CMD_RSCRATCHPAD		0xbe
#define DS1822_CMD_WSCRATCHPAD		0x4e
#define DS1822_CMD_CPYSCRATCHPAD	0x48
#define DS1822_CMD_RECEEPROM		0xb8
#define DS1822_CMD_RPWRSUPPLY		0xb4
#define DS1822_CMD_SEARCHROM		0xf0
#define DS1822_CMD_READROM			0x33
#define DS1822_CMD_MATCHROM			0x55
#define DS1822_CMD_SKIPROM			0xcc
#define DS1822_CMD_ALARMSEARCH		0xec
//-----------------------------------

void ds1822_choose_pin(unsigned char i)// [ok]
{
	i%=8;
	ds1822_pin=i;
}

void ds1822_input(void)// set ds1822 pin to input mode	[ok]
{
	DS1822_DIR&=~(1<<ds1822_pin);
}

void ds1822_output(void)// set ds1822 to output mode	[ok]
{
	DS1822_DIR|=(1<<ds1822_pin);
}

unsigned char ds1822_read_pin_stt(void)// read DS1822 status bit	[ok]
{
	ds1822_input();
	pin_stt=(DS1822_PIN&(1<<ds1822_pin))>>ds1822_pin;
	return pin_stt;
}

void ds1822_write_pin(unsigned char i)// write 1 or 0 to DQ line	[ok]
{
	ds1822_output();
	if(i==0)
	{
		DS1822_PORT&=~(1<<ds1822_pin);// write 0
	}	
	else
	{
		DS1822_PORT|=(1<<ds1822_pin);// write 1
	}
	
}

void ds1822_reset(void)	// [ok]
{
	//-------- pull down DQ_pin in 480us ---------
	ds1822_write_pin(0);
	_delay_us(480);
	//---------------------------------------------
	
	// release the line then wait for 60us to ensure
	// that ds1822 can caculate the call
	ds1822_input();
	_delay_us(60);
	//---------------------------------------------
	
	//store the value of DQ_line, then wait 420us
	//to complete the 480 us long
	ds1822_read_pin_stt();
	_delay_us(420);
	//----------------------------------------------
	// if pin_stt=0: ok; else fall
}

void master_write_bit(unsigned char i)
{
	if(i>1)i=1;
	ds1822_write_pin(0);// need 1us pull_down in DQ line in each individual write
	_delay_us(1);
	
	if(i==1)// if we want to write 1 to DQ_line, release it now
	{
		ds1822_input();
	}
	
	// else we wait to write 0 to DQ_line, wait at least 60us then release DQ_line
	_delay_us(60);
	ds1822_input();
}

void master_write_byte(unsigned char byte)// write LSB first, MSB last
{	
	unsigned char i;
	for(i=0;i<8;i++)
	{
		master_write_bit((byte&(1<<i))>>i);
	}
}

unsigned char master_read_bit(void)// read a bit in DQ_line
{
	unsigned char i;
	// pull down DQ_line in 1us
	ds1822_write_pin(0);
	_delay_us(1);
	
	// release the DQ_line and wait 19us to ensure the line is rally released
	ds1822_input();
	_delay_us(14);
	
	// read the DQ_line status then wait for 45us to end the write zero bit; 
	// write 1 bit no need to wait for 45us
	i=ds1822_read_pin_stt();// if i=0 we read as 0; if i=1 we read as 1
	_delay_us(45);
	
	return i;
}

unsigned char master_read_byte(void)// // read LSB first, MSB last
{
	unsigned char i=8, byte=0;
		while(i--)
		{
		//Shift one position right and store read value
		byte>>=1;
		byte|=(master_read_bit()<<7);
		}
	return byte;
}

void config_modreg(unsigned char i)//i= the numbers of resolution bits: throught 9 to 12
{
	if(i<9){i=9;}
		else if(i>12){i=12;}
			else{}					
	mod_reg= 0x1f|((i-9)<<5);	
}


/************************************************************************/
/* Read rom code                                          */
/************************************************************************/
void ds1822_read_rom_code(void)
{
	unsigned char i;
	ds1822_reset();
	master_write_byte(DS1822_CMD_READROM);
	
	for(i=0;i<8;i++)
	{
		rom_code[i]=master_read_byte();
	}
}

float ds1822_get_temp_single_sensor(void)
{	
	// reset, skip rom and start a conversion
	ds1822_reset();
	master_write_byte(DS1822_CMD_SKIPROM);
	master_write_byte(DS1822_CMD_CONVERTTEMP);
	
	// wait until a single conversion complete
	while(!master_read_bit());
	
	//Reset, skip ROM and send command to read Scratchpad
	ds1822_reset();
	master_write_byte(DS1822_CMD_SKIPROM);
	master_write_byte(DS1822_CMD_RSCRATCHPAD);
	
	// Read temp_l and temp_h byte
	temp_l=master_read_byte();
	temp_h=master_read_byte();
	ds1822_reset();
	
	// caculate the receive value
	integer=(temp_l>>4)|((temp_h&0x07)<<4);
	decimal=temp_l&0x0f;
	temp=((float) integer)+ ((float)decimal*decimal_step);
	return temp;
}

float ds1822_get_temp_multi_sensors(unsigned char sensor_number)
{
	unsigned char i;
	ds1822_choose_pin(0);
	ds1822_reset();
	master_write_byte(DS1822_CMD_MATCHROM);
	
	//----- send the correct rom code for each sensor -----
	for(i=0;i<8;i++)
	{
		master_write_byte(sensor_addr[sensor_number][i]);
	}
	//----------------------------------------------------
	
	master_write_byte(DS1822_CMD_CONVERTTEMP);// star a conversion
	while(!master_read_bit());// wait until a single conversion complete
	
	//--------------- read temperature data from specific ds18b20 ------
	ds1822_reset();// reset
	master_write_byte(DS1822_CMD_MATCHROM);// send match rom code command
	//----- send the correct rom code for each sensor -----
	for(i=0;i<8;i++)
	{
		master_write_byte(sensor_addr[sensor_number][i]);
	}
	//----------------------------------------------------	
	master_write_byte(DS1822_CMD_RSCRATCHPAD);
		
	// Read temp_l and temp_h byte
	temp_l=master_read_byte();
	temp_h=master_read_byte();
	ds1822_reset();	
	
	// caculate the receive value
	integer=(temp_l>>4)|((temp_h&0x07)<<4);
	decimal=temp_l&0x0f;
	temp=((float) integer)+ ((float)decimal*decimal_step);
	return temp;
}