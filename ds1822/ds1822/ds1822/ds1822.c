/*
 * ds1822.c
 *
 * Created: 6/6/2013 7:38:02 PM
 *  Author: Administrator
 */ 
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>


#include "ds1822.h"
#include "mpx4115.h"
#include "lcd20x4.h"
#include "button.h"
#include "control_heater.h"
#include "usart_ds1820.h"

#define sensor_number 4
#define debug
#define warm_up

unsigned char j,i,temp_pin_stt;
float t_temp[4];

int main(void)
{
	CLKPR=0x80;
	CLKPR=0x00;
	//--------- system init ------------
	cli();//prevent interrupt
	adc_init();
	lcd_init();
	button_init();
	heater_init();
	usart_init();
	//----------------------------------
	
	//---------- welcome note -----------------------	
	lcd_set_cs(1,5);
	lcd_send_string("Burn-in Box");
	lcd_set_cs(3,0);
	lcd_send_string("The VNPTtechnolgy ");
	lcd_set_cs(4,0);
	lcd_send_string("simulation product");
	//------------------------------------------------
	//--- let do some warm up measurement to against noise ---
	#ifdef warm_up
	for(j=0;j<2;j++)
	{
		for(i=0;i<sensor_number;i++)
		{
			ds1822_get_temp_multi_sensors(i);	
		}
	}
	    //----- getting temperature value ---
		for(i=0;i<sensor_number;i++)
		{
			ds1822_get_temp_multi_sensors(i);	
			if(temp<temp_error)
			{
			t_temp[i]=temp;
			}
			else{}
		}
		avg_temp=(float)(t_temp[0]+t_temp[1]+t_temp[2]+t_temp[3])/4;
		des_temp=(int)avg_temp;//destination temperature= present temperature, so we won't do anything
		//-----------------------------------
	#endif
	//--------------------------------------------------------
	sei();//enable interrupt
	heater_start_count();
	//--------------------------------------------------------	
    while(1)
    {
		if(settup==0)// measure temperature and control the heater to reach destination temperature
		{
		//----- getting pressure value ------
		adc_get_value(1);
		mpx4115_get_pressure();
		//-----------------------------------
		
		//----- getting temperature value ---
		for(i=0;i<sensor_number;i++)
		{
			ds1822_get_temp_multi_sensors(i);	
			if(temp<temp_error)
			{
			t_temp[i]=temp;
			}
			else{}
		}
		avg_temp=(float)(t_temp[0]+t_temp[1]+t_temp[2]+t_temp[3])/4;
		get_t_on();
		//-----------------------------------
			
		//----- display into lcd20x4--------
		lcd_clr();
		lcd_set_cs(1,0);
		lcd_send_string("Des_Temp: ");
		lcd_print_rnb(des_temp);
		lcd_send_string(" oC");
		lcd_set_cs(2,0);
		lcd_send_string("Temp: ");
		lcd_print_rnb(avg_temp);
		lcd_send_string(" oC");
		lcd_set_cs(3,0);
		lcd_send_string("Pressure: ");
		lcd_print_rnb(pressure);
		lcd_send_string("kPa");		
		//----------------------------------
		}
		else//set the destination temperature
		{
		lcd_clr();
		lcd_set_cs(1,0);
		lcd_send_string("Set destination temp");
		lcd_set_cs(2,5);
		lcd_print_rnb(des_temp);
		
		for(i=0;i<200;i++)
		{
			temp_pin_stt=get_button_stt(bt4);
		}
		if(temp_pin_stt==0)des_temp-=0.1;
		
		for(i=0;i<200;i++)
		{
			temp_pin_stt=get_button_stt(bt5);
		}
		if(temp_pin_stt==0)des_temp+=0.1;
		_delay_ms(200);
		}		
    }
}