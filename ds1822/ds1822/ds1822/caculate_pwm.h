
#define max_pulse_wide 5000
#define min_pule_wide 0
#define pulse_range 5000
unsigned long pwm_wide;
float contrain(float a,float min,float max)
{
	if(a<min){a=min;}
	if(a>max){a=max;}
}
void pwm_init(void)
{
	//------------- timer1 ----------------
	// t_step1= 0.5us, chia cho 8: oc1A/B=[min=1600(0.8ms); max=4000(2ms)]
	//TIMSK1|=1<<TOIE1;
	DDRB|=(1<<PINB1)|(1<<PINB2);//out put pwm pins
	ICR1=5000;// chu ki 2.5ms
	TCCR1A|=(1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);
	TCCR1B|=(1<<WGM13)|(1<<WGM12)|(1<<CS11);
	//--------------------------------------
	//------------- timer2 -----------------
	//t_step2=8us, chia cho 128: oc2A/B = [min=100(0.8ms); max=250(2ms)] 
	DDRB|=(1<<PINB3);//OC2A
	DDRD|=(1<<PIND3);//OC2B
	TCCR2A|=(1<<COM2A1)|(1<<COM2B1)|(1<<WGM21)|(1<<WGM20);
	TCCR2B|=(1<<CS22)|(1<<CS20);
	//--------------------------------------
}

void caculate_pwm_rate(float rate)// input: rate [%]; output: wide of pwm
{
	contrain(rate,0,1);
	pwm_wide= (unsigned int) pulse_range*rate;	
}

void pwm_fan(unsigned int pwm_wide)//t=[0:100] 
{
	OCR1A=pwm_wide;//OC1A, PB1
}

void pwm_heatter(unsigned int pwm_wide)//t=[0:100] 
{
	OCR1B=pwm_wide;//OC1B, PB2
}

