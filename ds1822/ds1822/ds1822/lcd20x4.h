#define LCDPORT PORTD
#define LCDDIR DDRD
#define LCDPIN PIND
#define LCD_DATA 0xf0
#define LCD_CONTROL 0x0e
#define PIN_RS 0x02
#define PIN_RW 0x04
#define PIN_E 0x08 
unsigned char lcd_stt,loop_clr;// bien luu trang thai cua lcd

//setting control bits are output
void CONTROL_OUT(void)
{
LCDDIR|=LCD_CONTROL;	
}

//setting data bits are input
void DATA_IN(void)
{
	LCDDIR &=(~LCD_DATA);
}


//setting data bits are output
void DATA_OUT(void)
{
	LCDDIR|=LCD_DATA;
}

//setting all bits are output
void all_dir_out(void)
{
	LCDDIR|=LCD_DATA|LCD_CONTROL;
}

//set or clr pinE
void pinE(unsigned char i)
{
	if(i==0){LCDPORT&= (~PIN_E);}
		else LCDPORT |= PIN_E;
}
//set or clr pinRW
void pinRW(unsigned char i)
{
	if(i==0){LCDPORT &= (~PIN_RW);}
		else LCDPORT |= PIN_RW;
}
//set or clr pinRS
void pinRS(unsigned char i)
{
	if(i==0){LCDPORT &= (~PIN_RS);}
		else LCDPORT|=PIN_RS;
}
//read status from LCD
void lcd_read_stt(void)
{
	DATA_IN();
	CONTROL_OUT();
	pinRW(1);
	pinRS(0);
	pinE(1);
	//_delay_ms(1);
	lcd_stt=LCDPIN&0xf0;//lay 4 bit trang thai cao truoc
	pinE(0);//chot du lieu dau ra
	//_delay_ms(1);
	pinE(1);
	//_delay_ms(1);
	lcd_stt|=(LCDPIN&0xf0)>>4;//lay not 4 bit du lieu thap
	pinE(0);
	
}
//wait when lcd is busy
void wait(void)
{    
	lcd_read_stt();
	while(lcd_stt&0x80){lcd_read_stt();}//khi bit7=1: lcd ban, doc trang thai tiep
	all_dir_out();//lcd roi, thoat
}

/*******************************************************************************
* Write 4-bits to LCD controller                                               *
*   Parameter:    c:      command to be written                                *
*   Return:                                                                    *
*******************************************************************************/
void lcd_write_4bit(unsigned char c)
{
	all_dir_out();
	pinRW(0);//ghi vao LCD
	pinE(1);
	LCDPORT &= (~LCD_DATA);//xoa 4 bit du lieu can ghi
	LCDPORT|= (c&0x0f)<<4;//ghi du lieu vao 4bit cao cua LCD
	_delay_ms(2);
	pinE(0);
	
}
/*******************************************************************************
* Write command to LCD controller                                              *
*   Parameter:    c:      command to be written                                *
*   Return:                                                                    *
*******************************************************************************/
void lcd_write_cmd(unsigned char c)
{     
	wait();
	pinRS(0);//chon thanh ghi lenh
	lcd_write_4bit(c>>4);//ghi 4 bit cao cua lenh truoc
	lcd_write_4bit(c);//ghi 4 bit thap cua lenh 	
}

/*******************************************************************************
* Write data to LCD controller                                                 *
*   Parameter:    c:      data to be written                                   *
*   Return:                                                                    *
*******************************************************************************/
void lcd_write_data(unsigned char c)
{
	wait();
	pinRS(1);//chon thanh ghi du lieu
	lcd_write_4bit(c>>4);//ghi 4 bit cao cua du lieu truoc
	lcd_write_4bit(c);//ghi 4 bit thap cua du lieu sau
}
/*******************************************************************************
* Print Character to current cursor position                                   *
*   Parameter:    c:      character to be printed                              *
*   Return:                                                                    *
*******************************************************************************/
void lcd_putchar(unsigned char c)
{
	lcd_write_data(c);
}
/*******************************************************************************
* Initialize the LCD controller                                                *
*   Parameter:                                                                 *
*   Return:                                                                    *
*******************************************************************************/
void lcd_init(void)
{	
  lcd_write_cmd (0x3); /* Select 4-bit interface             */
  _delay_ms(60); // tao tre
  lcd_write_cmd (0x3);   
  _delay_ms(60); // tao tre
  lcd_write_cmd (0x3);   
  _delay_ms(60); // tao tre
  lcd_write_cmd (0x2);	  //tro ve dau dong
  lcd_write_cmd (0x28);    /* 2 lines, 5x8 character matrix      */
  lcd_write_cmd (0x0C); //bat hien thi, tat con tro               
                         /* Display ctrl:Disp=ON,Curs/Blnk=OFF */
  lcd_write_cmd (0x06);                 /* Entry mode: Move right, no shift   */
  lcd_write_cmd(0x01);//xoa man hinh hien thi	
  lcd_write_cmd(0x80);
}
/*******************************************************************************
* Set cursor position on LCD display                                           *
*   Parameter:    column: column position                                      *
*                 line:   line position                                        *
*   Return:                                                                    *
*******************************************************************************/
void lcd_set_cs(unsigned char hang,unsigned char cot)
{
	unsigned char i;
	if(hang==1)
	{
		lcd_write_cmd(0x80);
	}//neu la hang 1 dua con tro ve dau dong thu nhat
	else if(hang==2)
	{
		lcd_write_cmd(0xc0);
	}
	else if(hang==3)
	{
		lcd_write_cmd(0x94);
	}	
	else 
	{
		lcd_write_cmd(0xD4);
	}
	for(i=0;i<cot;i++){lcd_write_cmd(0x14);}
}
/*******************************************************************************
* Xoa man hinh LCD                                              *
*******************************************************************************/
void lcd_clr(void)
{
	lcd_write_cmd(0x01);
}
/************************************************************************/
/* lcd send string                                                    */
/************************************************************************/
void lcd_send_string (unsigned char const *string)// lenh in 1 xau ki tu *string
{
  while (*string)
  {
    lcd_putchar(*string++); //in ra gia tri 1 ki tu cua xau, dc tro boi con tro *string
  }
}
/*******************************************************************************
* In so nguyen co 5 chu so                                                *
*******************************************************************************/
void lcd_print_nb(unsigned long n)//in so co 5 chu so ko co dau phay
{
	unsigned long a,b;
	if(n<10){lcd_putchar(n+48);}//so co 1 chu so
	  else 
	if(n<100)//so co 2 chu so
	{
		lcd_putchar(n/10+48);//in chu so hang chuc
		lcd_putchar(n%10+48);//in chu so hang don vi
	}
	
	  else
	if(n<1000)//so co 3 chu so
	{
		a=n/10;//a chua chu so hang tram va hang chuc
		lcd_putchar(a/10+48);//in chu so hang tram
		lcd_putchar(a%10+48);//in chu so hang chuc
		lcd_putchar(n%10+48);//in chu so hang don vi
		
	}
	 else if(n<10000)//so co 4 chu so 
	{
		a=n/100;//chua hang nghin va hang tram
		b=n%100;//chua hang chuc va don vi
		lcd_putchar(a/10+48);//in so hang nghin
		lcd_putchar(a%10+48);//in so hang tram
		lcd_putchar(b/10+48);//in so hang chuc
		lcd_putchar(b%10+48);//in so hang don vi
		
	}	
	 else if(n<100000)//so co 5 chu so
	 {
		a=n/1000;//chua hang chuc nghin va hang nghin
		b=n%1000;//chua hang tram,chuc va don vi
		lcd_putchar(a/10+48);//in so hang chuc nghin
		lcd_putchar(a%10+48);//in so hang nghin
		lcd_putchar(b/100+48);//in so hang tram
		lcd_putchar((b/10)%10+48);//in so hang don chuc
		lcd_putchar(b%10+48);//in so hang don vi
		
	 }		 
		
}

/*******************************************************************************
* In so co 3 chu so truoc dau phay, 3 chu so sau dau phay                                                *
*******************************************************************************/
void lcd_print_rnb(float r)//in so thap phan co 4 chu so sau dau phay
{
	unsigned long p_nguyen;
	unsigned int t_phan;
	
	if(r<0)// them dau tru neu la so am
	{
	lcd_putchar(45);
	r=-r;
	}
	
	p_nguyen=r/1;
	t_phan=(r-p_nguyen)*1000/1;//lay ba chu so sau dau phay, sau do in ra ca 3 ki tu so nay
	lcd_print_nb(p_nguyen);
	lcd_putchar(46);
	lcd_putchar((t_phan/100)+48);
	lcd_putchar((t_phan%100)/10+48);
	lcd_putchar(t_phan%10+48);
}

/************************************************************************/
/* Xuong dong                                                           */
/************************************************************************/
void lcd_xuongdong(void)
{
	lcd_putchar(13);
}


//-------- all convert to hexa then print  -------------
void lcd_print_hexnb(int x_var)
{
	unsigned char x[4],i;	
	//x[4]=(x_var&0xf0000)>>16;
	x[3]=(x_var&0xf000)>>12;
	x[2]=(x_var&0xf00)>>8;
	x[1]=(x_var&0xf0)>>4;
	x[0]=(x_var&0x0f);
	for(i=4;i>0;i--)
	{
		if(x[i-1]>10 || x[i-1]==10){lcd_putchar(x[i-1]+55);}
			else{lcd_putchar(x[i-1]+48);}
		//if(i==3){lcd_putchar(32);}
	}	
	
}

/************************************************************************/
/* lcd print int number to decimal type                               */
/************************************************************************/
void lcd_print_int(int a)
{
	if((a&0x8000)==0)//so duong
		{
			lcd_print_nb(a);
		}		
	else//so am
		{
			lcd_putchar(45);
			lcd_print_nb(65536-(unsigned int)a);
		}		
}