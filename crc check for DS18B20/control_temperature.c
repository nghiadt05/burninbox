#include <16F628.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES HS                       //High speed Osc (> 4mhz)
#FUSES NOPUT                    //No Power Up Timer
#FUSES PROTECT                  //Code protected from reads
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOMCLR                   //Master Clear pin used for I/O
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection

#use delay(clock=20000000)
#use rs232(baud=1200,parity=N,xmit=PIN_B2,rcv=PIN_B1,bits=8)

#byte PORTA = 0x05
#byte PORTB = 0x06
#byte TRISA = 0x85
#byte TRISB = 0x86

#byte T1CON  = 0x10
#byte TMR1L  = 0x0E
#byte TMR1H  = 0x0F
#byte INTCON = 0x0B
#byte PIE1   = 0x08C
#bit GIE = INTCON.7

#bit TMR1ON = T1CON.0
#include <DS18B20_B4.c>

unsigned int16 temp1=0, temp2=0,temp3=0,temp4= 0;
unsigned int16 sumtemp = 0;
unsigned char tempv = 0;
unsigned char tempc = 0;
const char table[256] =
{
 0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
 157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
 35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60,98,
 190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
 70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89,7,
 219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 
154,101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 
36, 248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 
185, 140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 
205, 17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14,
80, 175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 
238, 50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 
115, 202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 
139, 87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 
22, 233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 
168, 116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107,53};

char sensor_id_1[8]={0x28,0x3D,0xBF,0x9F,0x04,0x00,0x00,0x7C};
char sensor_id_2[8]={0x28,0xF5,0xF3,0x9F,0x04,0x00,0x00,0x8A}; 
char sensor_id_3[8]={0x28,0x89,0xC2,0x9D,0x04,0x00,0x00,0x21};
char sensor_id_4[8]={0x28,0xEA,0x78,0x9F,0x04,0x00,0x00,0x4E};
//////////////////////////////////////////////////////////////////
void setup12bit_18B20(char* ds_id) // set 12 bit resolution
{
int8 k;
//   disable_interrupts(INT_TIMER1);
//   ds18b20_write_byte(0xCC); //skip ROM
   ds18b20_write_byte(0x55); //Match ROM
   for (k =0;k<8;k++)
   {
     ds18b20_write_byte(ds_id);
   }
   ds18b20_write_byte(0x4E); //write 3 bytes of data to the DS18B20�s scratchpad
   ds18b20_write_byte(125); // write TH register value ; Alarm High threshold
   ds18b20_write_byte(-55); // write TL register value ; Alarm Low threshold
   ds18b20_write_byte(127); // write to Configutation register; 0x7F = 12 bit 
   ds18b20_initialization(); //resolution, 750 ms conversion time
   //ds18b20_write_byte(0xCC); // skip ROM
   ds18b20_write_byte(0x55); //Match ROM
   for (k =0;k<8;k++)
   {
     ds18b20_write_byte(ds_id);
   }
   ds18b20_write_byte(0x48); // COPY SCRATCHPAD to EEPROM
 //  enable_interrupts(INT_TIMER1);
   delay_ms(15); 
}
///////////////////////////////////////////////////////////////////
unsigned int16 read_18B20(char* id)
{
int8 k,i,CRC; 
int8 scratchpad_data[9]; 
signed int16 temp=0x0000;
unsigned int16 degree,tenth;
loop:
   for(i=0; i<9; i++) scratchpad_data[i]=0x00; // Clear scratchpad_data
// disable_interrupts(INT_TIMER1);
   if(ds18b20_initialization())
   {
   ds18b20_write_byte(0x55); //Match ROM
   for (k =0;k<8;k++)
   {
     ds18b20_write_byte(id[k]);
   }
   //ds18b20_write_byte(0xCC);  //skip ROM command
   ds18b20_write_byte(0x44);  //convert temperature command
   output_high(DS18B20_IO);
 //  enable_interrupts(INT_TIMER1);
   delay_ms(800);             //max conversion time for 12bit resolution=750ms
   output_float(DS18B20_IO);
//   disable_interrupts(INT_TIMER1);
   ds18b20_initialization(); 
   ds18b20_write_byte(0x55); //Match ROM
   for (k =0;k<8;k++)
   {
     ds18b20_write_byte(id[k]);
   }
 //  ds18b20_write_byte(0xCC);  //skip ROM command
   ds18b20_write_byte(0xBE);  //read scratchpad command 
//   enable_interrupts(INT_TIMER1);
     CRC = 0;
      for(i=0; i<9; i++)
      { 
         scratchpad_data[i]=ds18b20_read_byte();
         CRC = table[CRC ^ scratchpad_data[i]];
      } 
      if(CRC == 0) // No error 
      {
      temp = make16(scratchpad_data[1], scratchpad_data[0]);
      temp = temp & 0x07FF; // calculate the whole number part 
      degree = (temp >> 4); // integral part
      tenth = scratchpad_data[0] & 0x0F; 
      tenth=((unsigned int16)(tenth) * 0x0A) >> 4; // decimal part
      degree = degree * 10 + tenth;
      }
      else
      goto loop;
     } 
return degree;     
}
//////////////////////////////////////////////////////////////////
/*
#int_timer1
void ngattimer1()
{
    TMR1ON=0;
    TMR1L = 0xAF;
    TMR1H = 0x3C;// 10ms
    TMR1ON = 1;
    time++;
    if(time >=100)// cu 1s thi gui mot lan ket qua doc duoc
    {
        Readcount ++;
        if(ReadCount > 5) ReadCount=0;
        time =0;
   }
}

void Init_Main()
{
    TRISB = 0x00;
    TRISA = 0x00;
//    INTCON = 0b11000000;
//    PIE1   = 0b00000001;
    output_high(ONE_WIRE_PIN);
    delay_ms(250);
    setup12bit_18B20(sensor_id_1);
    
    output_high(ONE_WIRE_PIN);
    delay_ms(250);
    setup12bit_18B20(sensor_id_2);
    
    output_high(ONE_WIRE_PIN);
    delay_ms(250);
    setup12bit_18B20(sensor_id_3);
    
    output_high(ONE_WIRE_PIN);
    delay_ms(250);
    setup12bit_18B20(sensor_id_4);
}
*/
void main()
{
    TRISB = 0x00;
    TRISA = 0x00;
//  INTCON = 0b11000000;
//  PIE1   = 0b00000001;
    output_high(DS18B20_IO);
    delay_ms(350);
    setup12bit_18B20(sensor_id_1);
    
    output_high(DS18B20_IO);
    delay_ms(350);
    setup12bit_18B20(sensor_id_2);
    
    output_high(DS18B20_IO);
    delay_ms(350);
    setup12bit_18B20(sensor_id_3);
    
    output_high(DS18B20_IO);
    delay_ms(350);
    setup12bit_18B20(sensor_id_4);
    
while(1)
   {

   temp1 = read_18B20(sensor_id_1);
   /*
   tempc = (unsigned char)(temp1 / 10);
   tempv = (unsigned char)(temp1 % 10);
   putc(tempc);
   delay_ms(30);
   putc(tempv);
*/
   temp2= read_18B20(sensor_id_2);
   /*
   tempc = (unsigned char)(temp2 / 10);
   tempv = (unsigned char)(temp2 % 10);
   putc(tempc);
   delay_ms(30);
   putc(tempv);
*/
   temp3= read_18B20(sensor_id_3);
   /*
   tempc = (unsigned char)(temp3 / 10);
   tempv = (unsigned char)(temp3 % 10);
   putc(tempc);
   delay_ms(30);
   putc(tempv);
*/
   temp4 = read_18B20(sensor_id_4);
   /*
   tempc = (unsigned char)(temp4 / 10);
   tempv = (unsigned char)(temp4 % 10);
   putc(tempc);
   delay_ms(30);
   putc(tempv);
   delay_ms(1000);
 */
   sumtemp = temp1;
   sumtemp += temp2;
   sumtemp += temp3;
   sumtemp += temp4;
   sumtemp >>= 2; // divide by 4
   
   tempc = (unsigned char)(sumtemp / 10);
   tempv = (unsigned char)(sumtemp % 10);
   putc(tempc);
   delay_ms(30);
   putc(tempv);
   delay_ms(1000);
/*
   delay_ms(1000);
   putc(11);
   delay_ms(30);
   putc(1);
   delay_ms(1000);
   */
   }
}
